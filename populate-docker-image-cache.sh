#!/bin/bash

docker pull calico/node:v3.8.9  
docker pull calico/pod2daemon-flexvol:v3.8.9  
docker pull calico/cni:v3.8.9  
docker pull calico/kube-controllers:v3.8.9
docker pull k8s.gcr.io/kube-proxy:v1.16.10
docker pull k8s.gcr.io/kube-apiserver:v1.16.10
docker pull k8s.gcr.io/kube-controller-manager:v1.16.10
docker pull k8s.gcr.io/kube-scheduler:v1.16.10
docker pull k8s.gcr.io/etcd:3.3.15-0
docker pull quay.io/prometheus/node-exporter:v0.18.1 
docker pull quay.io/coreos/kube-rbac-proxy:v0.4.1  
docker pull k8s.gcr.io/pause:3.1     
docker pull amazon/aws-cli:2.0.8
docker pull kumori/outofservice:node10-minimal
docker pull jimmidyson/configmap-reload:v0.3.0
docker pull grafana/grafana:6.4.3
docker pull kubernetesui/dashboard:v2.0.0-rc5
docker pull jboss/keycloak:6.0.1
docker pull kubernetesui/metrics-scraper:v1.0.3


docker tag calico/node:v3.8.9                            registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9  
docker tag calico/pod2daemon-flexvol:v3.8.9              registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9  
docker tag calico/cni:v3.8.9                             registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9  
docker tag calico/kube-controllers:v3.8.9                registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9
docker tag k8s.gcr.io/kube-proxy:v1.16.10                registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker tag k8s.gcr.io/kube-apiserver:v1.16.10            registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10
docker tag k8s.gcr.io/kube-controller-manager:v1.16.10   registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10
docker tag k8s.gcr.io/kube-scheduler:v1.16.10            registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10
docker tag k8s.gcr.io/etcd:3.3.15-0                      registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0
docker tag quay.io/prometheus/node-exporter:v0.18.1      registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1 
docker tag quay.io/coreos/kube-rbac-proxy:v0.4.1         registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1  
docker tag k8s.gcr.io/pause:3.1                          registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1     
docker tag amazon/aws-cli:2.0.8                          registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8
docker tag kumori/outofservice:node10-minimal            registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal
docker tag jimmidyson/configmap-reload:v0.3.0            registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0
docker tag grafana/grafana:6.4.3                         registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3
docker tag kubernetesui/dashboard:v2.0.0-rc5             registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5
docker tag jboss/keycloak:6.0.1                          registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1
docker tag kubernetesui/metrics-scraper:v1.0.3           registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3


docker push registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9  
docker push registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9  
docker push registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9  
docker push registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0
docker push registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1 
docker push registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1  
docker push registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1     
docker push registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8
docker push registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal
docker push registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0
docker push registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3
docker push registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5
docker push registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1
docker push registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3


docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-apiserver:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-controller-manager:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-scheduler:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-etcd:3.3.15-0
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1 
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1     
docker rmi registry.gitlab.com/kumori-systems/docker-cache/amazon-aws-cli:2.0.8
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal
docker rmi registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0
docker rmi registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5
docker rmi registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3
docker rmi calico/node:v3.8.9                          
docker rmi calico/pod2daemon-flexvol:v3.8.9            
docker rmi calico/cni:v3.8.9                           
docker rmi calico/kube-controllers:v3.8.9      
docker rmi k8s.gcr.io/kube-proxy:v1.16.10              
docker rmi k8s.gcr.io/kube-apiserver:v1.16.10          
docker rmi k8s.gcr.io/kube-controller-manager:v1.16.10 
docker rmi k8s.gcr.io/kube-scheduler:v1.16.10          
docker rmi k8s.gcr.io/etcd:3.3.15-0                    
docker rmi quay.io/prometheus/node-exporter:v0.18.1    
docker rmi quay.io/coreos/kube-rbac-proxy:v0.4.1       
docker rmi k8s.gcr.io/pause:3.1                        
docker rmi amazon/aws-cli:2.0.8                        
docker rmi kumori/outofservice:node10-minimal  
docker rmi jimmidyson/configmap-reload:v0.3.0  
docker rmi grafana/grafana:6.4.3               
docker rmi kubernetesui/dashboard:v2.0.0-rc5   
docker rmi jboss/keycloak:6.0.1                
docker rmi kubernetesui/metrics-scraper:v1.0.3 
