#!/bin/bash

docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9  
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9  
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9  
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker pull registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1  
docker pull registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1     
docker pull registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1 
docker pull registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal
docker pull registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0
docker pull registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3
docker pull registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5
docker pull registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1
docker pull registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3


docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9                           calico/node:v3.8.9                            
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9             calico/pod2daemon-flexvol:v3.8.9              
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9                            calico/cni:v3.8.9                             
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9               calico/kube-controllers:v3.8.9       
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10               k8s.gcr.io/kube-proxy:v1.16.10                
docker tag registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1     quay.io/prometheus/node-exporter:v0.18.1      
docker tag registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1        quay.io/coreos/kube-rbac-proxy:v0.4.1         
docker tag registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1                         k8s.gcr.io/pause:3.1                          
docker tag registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal           kumori/outofservice:node10-minimal   
docker tag registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0           jimmidyson/configmap-reload:v0.3.0   
docker tag registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3                        grafana/grafana:6.4.3                
docker tag registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5            kubernetesui/dashboard:v2.0.0-rc5    
docker tag registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1                         jboss/keycloak:6.0.1                 
docker tag registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3          kubernetesui/metrics-scraper:v1.0.3  


docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-pod2daemon-flexvol:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-cni:v3.8.9  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-kube-controllers:v3.8.9     
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-kube-proxy:v1.16.10
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-prometheus-node-exporter:v0.18.1 
docker rmi registry.gitlab.com/kumori-systems/docker-cache/quay.io-coreos-kube-rbac-proxy:v0.4.1  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/k8s.gcr.io-pause:3.1     
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kumori-outofservice:node10-minimal 
docker rmi registry.gitlab.com/kumori-systems/docker-cache/jimmidyson-configmap-reload:v0.3.0 
docker rmi registry.gitlab.com/kumori-systems/docker-cache/grafana-grafana:6.4.3              
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-dashboard:v2.0.0-rc5  
docker rmi registry.gitlab.com/kumori-systems/docker-cache/jboss-keycloak:6.0.1               
docker rmi registry.gitlab.com/kumori-systems/docker-cache/kubernetesui-metrics-scraper:v1.0.3
