# Docker Cache

Docker registry containing all necessary images for creating Kumori Platform
clusters.

:warning: **06/10/2022: images have been removed from this repository registry to free up space.**

## Contents for Kumori Platform distribution v1.x

Copies of the following docker images are stored in the regsitry under Kumori
custom names (to be able to store them properly in GitLab).

All local images names are prefixed with
`registry.gitlab.com/kumori-systems/docker-cache/`.

```
ORIGINAL IMAGE NAME                             KUMORI IMAGE NAME (in this cache)
===========================================================================================
calico/node:v3.8.9                              calico-node:v3.8.9
calico/pod2daemon-flexvol:v3.8.9                calico-pod2daemon-flexvol:v3.8.9
calico/cni:v3.8.9                               calico-cni:v3.8.9
calico/kube-controllers:v3.8.9                  calico-kube-controllers:v3.8.9
k8s.gcr.io/kube-proxy:v1.16.10                  k8s.gcr.io-kube-proxy:v1.16.10
k8s.gcr.io/kube-apiserver:v1.16.10              k8s.gcr.io-kube-apiserver:v1.16.10
k8s.gcr.io/kube-controller-manager:v1.16.10     k8s.gcr.io-kube-controller-manager:v1.16.10
k8s.gcr.io/kube-scheduler:v1.16.10              k8s.gcr.io-kube-scheduler:v1.16.10
k8s.gcr.io/etcd:3.3.15-0                        k8s.gcr.io-etcd:3.3.15-0
quay.io/prometheus/node-exporter:v0.18.1        quay.io-prometheus-node-exporter:v0.18.1
quay.io/coreos/kube-rbac-proxy:v0.4.1           quay.io-coreos-kube-rbac-proxy:v0.4.1
k8s.gcr.io/pause:3.1                            k8s.gcr.io-pause:3.1
amazon/aws-cli:2.0.8                            amazon-aws-cli:2.0.8
kumori/outofservice:node10-minimal              kumori-outofservice:node10-minimal
jimmidyson/configmap-reload:v0.3.0              jimmidyson-configmap-reload:v0.3.0
grafana/grafana:6.4.3                           grafana-grafana:6.4.3
kubernetesui/dashboard:v2.0.0-rc5               kubernetesui-dashboard:v2.0.0-rc5
jboss/keycloak:6.0.1                            jboss-keycloak:6.0.1
kubernetesui/metrics-scraper:v1.0.3             kubernetesui-metrics-scraper:v1.0.3
```

## Cache creation script

A very **basic script for pulling the original images, tagging them with the
appropriate name and pushing them to the registry** is included in the
repository:

**`populate-docker-image-cache.sh`**

The script can be run from any machine with docker installed and **requires
being logged in to GitLab registry with valid credentials for this repository**.

If we take one image as an example, the script will perform these actions:
```
# Pull the original image
docker pull calico/node:v3.8.9

# Tag the image with the local name
docker tag calico/node:v3.8.9  registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9

# The next action assumes you are logged in to the repository registry with
# valid credentials.

# Push the image to the cache registry
docker push registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9

# Delete the image from the local machine
docker rmi calico/node:v3.8.9
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9
```

**NOTE:** This is first version and could clearly be improved.


## Sample preload scripts

**Sample scripts for preloading** the docker images in Kumori cluster nodes are
also included for reference. Kumori Installer can be configured to
automatically preload the images, so using these scripts is not required.

**Preload doesn't require registry authentication (the registry is public)**.

For each image, the preload script will perform these actions:
```
# Pull the image from the cache
docker pull registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9

# Tag the image with its original name
docker tag registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9 calico/node:v3.8.9

# Delete the cache name tag
docker rmi registry.gitlab.com/kumori-systems/docker-cache/calico-node:v3.8.9
```
